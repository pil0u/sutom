import Configuration from "./entites/configuration";
import PanelManager from "./panelManager";
import Sauvegardeur from "./sauvegardeur";

export default class ReglesPanel {
  private readonly _panelManager: PanelManager;
  private readonly _rulesBouton: HTMLElement;

  public constructor(panelManager: PanelManager) {
    this._panelManager = panelManager;
    this._rulesBouton = document.getElementById("configuration-regles-bouton") as HTMLElement;

    this._rulesBouton.addEventListener(
      "click",
      (() => {
        this.afficher();
      }).bind(this)
    );
  }

  public afficher(): void {
    let titre = "Flash info";
    let contenu =
      "<p>" +
      "Le 20 janvier 2023, Gabrielle & Pierre-Loïc sont devenus les parents d'un petit garçon.<br /><br />" +
      "Il a déjà beaucoup de cheveux et une bouille craquante ♥️<br /><br />" +
      "Mais comment s'appelle-t-il ?<br /><br />" +
      "Pour le découvrir, prêtez-vous à cette petite énigme, inspirée du célèbre jeu télévisé MOTUS.<br />" +
      "Vous avez six essais pour deviner le prénom, qui contient 7 lettres.<br /><br />" +
      "Bonne chance !" +
      "</p>" +
      '<div class="grille">' +
      "<table>" +
      "<tr>" +
      '<td class="resultat bien-place">E</td>' +
      '<td class="resultat mal-place">X</td>' +
      '<td class="resultat bien-place">E</td>' +
      '<td class="resultat non-trouve">M</td>' +
      '<td class="resultat non-trouve">P</td>' +
      '<td class="resultat mal-place">L</td>' +
      '<td class="resultat mal-place">E</td>' +
      "</tr>" +
      "</table>" +
      "<br />Les lettres entourées d'un carré rouge sont bien placées,<br />" +
      "les lettres entourées d'un cercle jaune sont mal placées (mais présentes dans le mot).<br />" +
      "Les lettres qui restent sur fond bleu ne sont pas dans le mot.<br /><br />" +
      'Basé sur l\'excellent <a target="_blank" href="https://sutome.nocle.fr/">Sutom</a> auquel vous pouvez jouer tous les jours avec un nouveau mot.<br />' +
      "</div>";

    this._panelManager.setContenu(titre, contenu);
    this._panelManager.setClasses(["regles-panel"]);
    this._panelManager.setCallbackFermeture(() => {
      Sauvegardeur.sauvegarderConfig({
        ...(Sauvegardeur.chargerConfig() ?? Configuration.Default),
        afficherRegles: true,
      });
    });
    this._panelManager.afficherPanel();
  }
}
